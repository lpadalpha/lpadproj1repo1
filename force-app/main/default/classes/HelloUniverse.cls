public with sharing class HelloUniverse {
    public HelloUniverse() {
        system.debug('Hello Universe from user1');
        system.debug('Hello Universe from user2');
    }
}