public with sharing class HelloWorld {
    public HelloWorld() {
        system.debug('Hello World from user1 of coresphere');
        system.debug('Hello World from user2');
    }
}
